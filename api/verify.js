const { send } = require('micro');
const jwt = require('jsonwebtoken');

const SECRET_KEY = 'irejgoisejrhposierfpoisjervoiusenrtgpinOUIGOIHPORNPOITNH8949oweurng98'; // Replace with your secret key

module.exports = async (req, res) => {
  if (req.method === 'GET') { // Change the method check to 'GET'
    try {
      // Retrieve the token from the 'Authorization' header
      const token = req.headers.authorization ? req.headers.authorization.replace('Bearer ', '') : null;

      if (!token) {
        send(res, 401, { error: 'Authorization token missing' });
        return;
      }

      // Verify the JWT token
      jwt.verify(token, SECRET_KEY, (err, decoded) => {
        if (err) {
          send(res, 401, { error: 'Invalid token' });
        } else {
          send(res, 200, { message: 'Token is valid', username: decoded.username });
        }
      });
    } catch (error) {
      console.error('Error:', error);
      send(res, 500, { error: 'An error occurred.' });
    }
  } else {
    send(res, 405, { error: 'Method not allowed.' });
  }
};
