let body = ''; // Initialize body as an empty string

export default async function handler(req, res) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  if (req.method === 'POST') {
    try {
      const { data } = req.body;
      body = data; // Store data in the body variable
      res.status(200).json({ username: data });
    } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ error: 'An error occurred.' });
    }
  } else if (req.method === 'GET') {
    try {
      // Send the response back to the client
      res.status(200).json({ username: body });
    } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ error: 'An error occurred.' });
    }
  } else {
    res.status(405).json({ error: 'Method not allowed.' });
  }
}
