let body = ''; // Initialize body as a global variable

module.exports = async function handler(req, res) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  if (req.method === 'POST') {
    try {
      const conditions = ["nig"]; // Disallowed words
      const { data } = req.body;
      body = data; // Store data in the global variable
      if (conditions.some(el => body.includes(el))) {
        res.status(403).json({ error: 'This message contains forbidden word(s).' });
      } else {
        res.status(200).json({ message: data });
      }
    } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ error: 'An error occurred.' });
    }
  } else if (req.method === 'GET') {
    try {
      const conditions = ["nig"]; // Disallowed words
      if (conditions.some(el => body.includes(el))) {
        res.status(403).json({ error: 'This message contains forbidden word(s).' });
      } else {
        res.status(200).json({ message: body });
      }
    } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ error: 'An error occurred.' });
    }
  } else {
    res.status(405).json({ error: 'Method not allowed.' });
  }
};
