const { send } = require('micro');
const jwt = require('jsonwebtoken');

const SECRET_KEY = 'irejgoisejrhposierfpoisjervoiusenrtgpinOUIGOIHPORNPOITNH8949oweurng98'; 


const users = [
  { username: 'jackson', password: 'Microbot13#13Metnals' }, //jackson
  { username: 'wellerman', password: 'ohiorizzler' }, //wesley
  { username: 'gummy', password: 'Jackson<3' }, //adam
];

module.exports = async (req, res) => {
  if (req.method === 'POST') {
    try {
      const { username, password } = req.body;

      // Check if username and password match any user in the array
      const matchedUser = users.find(user => user.username === username && user.password === password);

      if (matchedUser) {
        // Generate a JWT token
        const token = jwt.sign({ username }, SECRET_KEY, { expiresIn: '1h' });

        send(res, 200, { token });
      } else {
        send(res, 401, { error: 'Invalid credentials' });
      }
    } catch (error) {
      console.error('Error:', error);
      send(res, 500, { error: 'An error occurred.' });
    }
  } else {
    send(res, 405, { error: 'Method not allowed.' });
  }
};
