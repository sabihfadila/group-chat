const signInForm = document.getElementById('signInForm');
signInForm.addEventListener('submit', handleSignIn);

async function handleSignIn(event) {
  event.preventDefault();

  const username = document.getElementById('username').value;
  const password = document.getElementById('password').value;

  try {
    const response = await fetch('/api/login.js', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    });

    if (response.ok) {
      const data = await response.json();
      // Save the username and token in cookies
      Cookies.set('username', username);
      Cookies.set('token', data.token);
      // Redirect to a protected page or perform other actions
      window.location.href = './chat/'; // Replace with your protected page
    } else {
      const errorData = await response.text(); // Read the response as text
      console.error('Login failed:', errorData);
      document.getElementById('failed').innerHTML = 'Login failed: ' + errorData;
      document.getElementById('failed').style.opacity = 1;
    }
  } catch (error) {
    console.error('Error:', error);
  }
}
