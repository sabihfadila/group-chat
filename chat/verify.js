let token;
getCookie('token');
function getCookie(cookieName) {
    const cookies = document.cookie.split(';');

    for (const cookie of cookies) {
      const [name, value] = cookie.trim().split('=');
  
      // Check if the current cookie's name matches the desired cookieName
      if (name === cookieName) {

        return decodeURIComponent(value);
      }
    }

    return null;
  }

// Function to make authenticated requests
async function makeAuthenticatedRequest() {
    const token = getCookie('token'); // Implement a function to retrieve the token
  
    try {
      const response = await fetch('/api/verify.js', {
        method: 'GET', // or 'POST' for a POST request
        headers: {
          'Authorization': `Bearer ${token}`, // Attach the token in the 'Authorization' header
          'Content-Type': 'application/json',
        },
        // Additional request options (e.g., body for POST requests)
      });
  
      if (response.ok) {
        const data = await response.json();
        console.log('Server response:', data);
      } else {
        console.error('Server error:', response.status, response.statusText);
        window.location.href = '/'
      }
    } catch (error) {
      console.error('Error:', error);
      window.location.href = '/'
    }
  }
  makeAuthenticatedRequest();