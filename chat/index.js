let titlecnt = 1;
let cookies = document.cookie.split(';').map(item => item.split('=')).reduce((acc, [k, v]) => (acc[k.trim().replace('"', '')] = v) && acc, {});cookies = document.cookie.split(';').map(item => item.split('=')).reduce((acc, [k, v]) => (acc[k.trim().replace('"', '')] = v) && acc, {});cookies = document.cookie.split(';').map(item => item.split('=')).reduce((acc, [k, v]) => (acc[k.trim().replace('"', '')] = v) && acc, {});cookies = document.cookie.split(';').map(item => item.split('=')).reduce((acc, [k, v]) => (acc[k.trim().replace('"', '')] = v) && acc, {});
let contents1;
let username1;
let contents;
let username;
let addNew = true;
let lastAddedCont;

function scrollDown() {
    var elem = document.getElementById('contain');
    elem.scrollTop = elem.scrollHeight;
}

var wage = document.getElementById("message");
wage.addEventListener("keydown", function (e) {
    if (e.code === "Enter") {  //checks whether the pressed key is "Enter"
        validate(e);
    }
});

function validate(e) {
    contents = document.getElementById('message').value;
    username = cookies.username;
    if (contents == '') {
        contents = 'Empty message'
    }
    console.log('Requesting add new message with permametres: ' + username + ',' + contents)
    addNew = false;
    //add scripts about querying the database ._.
    if (window.location.href.includes('127.0.0.1') == true) {
      addNew = true;
      addNewMessage(username, contents)
    } else {
      updateServerlessFunction(username, contents);
    }
    //end
    document.getElementById('message').value = '';
}

function addNewMessage(auth, cont) {
  if (addNew === true) {
    if (auth == '' || cont == '') {
      console.log('Message was denied as it was unintentional')
    } else {
      lastAddedCont = cont;
      var elem = document.getElementById('cloneme')
      var clone = elem.cloneNode(true);
      elem.id = 'no-more'
      elem.class = 'messagething'

      var conts = clone.querySelector('#contents')
      var author = clone.querySelector('#author')
      var time = clone.querySelector('#timestamp')
      const d = new Date();
      let ampm = d.getHours();
      let min = d.getMinutes();
      if (ampm > 12) {
          let ampm = " PM"
          console.log("PM")
          if (min < 10) {
            time.innerHTML = d.getHours()-12 + ":" + "0" + d.getMinutes() + ampm;
          } else {
            time.innerHTML = d.getHours()-12 + ":" + d.getMinutes() + ampm;
          }
          
      }
      if (ampm < 12) {
           ampm = " AM"
          console.log("AM")
          console.log(ampm)
          if (min < 10) {
            time.innerHTML = d.getHours() + ":" + "0" + d.getMinutes() + ampm;
          } else {
            time.innerHTML = d.getHours() + ":" + d.getMinutes() + ampm;
          }
      }


      conts.innerHTML = cont;
      author.innerHTML = auth;

      elem.after(clone);
      scrollDown();

      document.title = ' (' + titlecnt + ') | Metnals Hsosptial'
      titlecnt++
      console.log('added new message with data: ' + auth + ',' + cont + ',titlecnt:' + titlecnt)
    }
  }
}

function updateServerlessFunction(username, message) {
    // Send the input data to the serverless function using a fetch request
    fetch('https://metnals-hospital.vercel.app/api/updateDataBody.js', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ data: message }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => {
        console.log('Serverless function response:', data);
        addNew = false;
      })
      .catch((error) => {
        console.error('Error:', error);
      });

      fetch('https://metnals-hospital.vercel.app/api/updateDataUser.js', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ data: username }),
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((data) => {
          console.log('Serverless function response:', data);
          addNew = false;
        })
        .catch((error) => {
          console.error('Error:', error);
        }); 
  }

  function fetchData() {
    const apiUrlUser = 'https://metnals-hospital.vercel.app/api/updateDataUser.js';
    const apiUrlBody = 'https://metnals-hospital.vercel.app/api/updateDataBody.js';
  
    // Create a function that fetches data and returns a promise
    function fetchDataFromApi(apiUrl) {
      return fetch(apiUrl)
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json(); // Parse the JSON response
        });
    }
  
    // Fetch data for user and body
    const userPromise = fetchDataFromApi(apiUrlUser);
    const bodyPromise = fetchDataFromApi(apiUrlBody);
  
    // Use Promise.all to wait for both promises to resolve
    return Promise.all([userPromise, bodyPromise])
      .then(([userData, bodyData]) => {
        // Now you have access to both userData and bodyData
        console.log('User data:', userData);
        console.log('Body data:', bodyData);
  
        // Assign the data to the contents1 variable
        contents1 = bodyData
        username1 = userData
  
        return contents1;
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  }


  setInterval(addMessageMaybe, 500);
  function addMessageMaybe() {
    if (window.location.href.includes('127.0.0.1')) {

    } else {
      fetchData();
      if (contents1.message == contents) {
        console.log('no new messages.')
      } else {
        addNew = true;
        contents = contents1.message
        username = username1.username
        console.log('new message!')
        if (contents == lastAddedCont) {
          console.log('Message was denied')
        } else {
          addNewMessage(username1.username, contents1.message)
        }
      }
    }
  }